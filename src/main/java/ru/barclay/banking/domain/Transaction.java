package ru.barclay.banking.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@NoArgsConstructor(access= AccessLevel.PRIVATE, force=true)
@Entity
public class Transaction {

    @Id
    private String accountNumber;
    private String type;
    private String currency;
    private BigDecimal amount;
    private String merchantName;
    private String merchantLogo;

    public  class Builder {
        public Builder wtithAccountNumber(String accountN) {
            Transaction.this.accountNumber = accountN;
            return this;
        }

        public Transaction build() {
            return Transaction.this;
        }
    }

    public static Builder builder() {
        return new Transaction().new Builder();
    }
}



