package ru.barclay.banking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.barclay.banking.domain.Transaction;
import ru.barclay.banking.services.TransactionService;

import java.util.List;

@RestController
@RequestMapping(path="/transactions", produces = "application/json")
public class TransactionController {

    @Autowired
    private TransactionService service;

    @GetMapping("/getall/{account}")
    public List<Transaction> getAllByAccount(@PathVariable("account") Long accountNumber) {
        return service.findAllByAccountNumber(accountNumber.toString());
    }
}
