package ru.barclay.banking.controller;

import io.swagger.model.OBReadTransaction6;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.barclay.banking.data.TransactionRepository;
import ru.barclay.banking.domain.Transaction;
import ru.barclay.banking.services.TransactionService;

@RestController
@RequestMapping(path = "/accounts", produces = "application/json")
public class OBTransactionAdapter {

    private final TransactionService service;

    @Autowired
    public OBTransactionAdapter(TransactionService transactionService) {
        this.service = transactionService;
    }

    @GetMapping("/{account}/transactions")
    public OBReadTransaction6 getTransaction(@PathVariable String accountId) {

        return null;
    }
}
