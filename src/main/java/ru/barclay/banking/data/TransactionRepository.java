package ru.barclay.banking.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.barclay.banking.domain.Transaction;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, String> {
}
