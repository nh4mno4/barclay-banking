package ru.barclay.banking.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.barclay.banking.domain.Transaction;

import java.util.List;

@SpringBootTest
class TransactionSeviceTest {

    @Autowired
    private TransactionService service;

    @Test
    void findAllByAccountTest() {
        List<Transaction> result = service.findAllByAccountNumber("1234");
        Assertions.assertTrue(result.size() > 0);
    }
}
