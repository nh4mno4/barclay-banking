package ru.barclay.banking.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.barclay.banking.data.TransactionRepository;
import ru.barclay.banking.domain.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TransactionService {

    private final TransactionRepository repository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.repository = transactionRepository;
    }

    public List<Transaction> findAllByAccountNumber(String account) {
        return IntStream.rangeClosed(1,10).mapToObj(t-> Transaction.builder()
                .wtithAccountNumber(account).build())
                .collect(Collectors.toList());
    }
}
